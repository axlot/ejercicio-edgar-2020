'use strict'

let totalUI = document.getElementById('total');
let total = 0;
const daysTabs = document.getElementById('nav-tab');

getTasks();
daysTabs.addEventListener('click', onTabClick, false);


//It is in charge of displaying the information in the UI
function talksUI(talks){
    for (const key in talks){
        let div = document.getElementById(key);

        talks[key].forEach(task => {
            let id = Math.random().toString(36).substring(7);
            let isCompleted = task.completed ? 'completed': '';
            let checkbox;

            if(task.completed){
                total += task.points;
                totalUI.innerHTML = `$${total}`;
                checkbox = 'checked';
            }else{
                checkbox = null;
            }
            
            div.innerHTML += `
                <div class="item ${isCompleted}">
                    <div class="data-item">
                        <div class="icon">
                            <img src="img/${task.icon}" class="icon-task" alt="">
                        </div>
                        <div class="data">
                            <h3>${task.task}</h3>
                            <p>${task.category}</p>
                        </div>
                    </div>
                    <input type="checkbox" id=${id} value=${task.points} ${checkbox} onClick="checkbox(this)"/>
                    <label for=${id}></label>    
                </div>  
            `;
        })
    }
}

//Controls the checkboxs, if sum is activated, if it is deactivated subtract
function checkbox(e){ 
    if(e.checked){
        total+= Number(e.value);
        e.parentElement.className += ' completed';
    }else{
        total -= Number(e.value);
        e.parentElement.className = e.parentElement.className.replace('completed', '');
    }
    totalUI.innerHTML = `$${total}`;
}


//Getting data of tareas.json
function getTasks(){
    //Take CORS into account
    fetch('tareas.json')
    .then(resp => resp.json())
    .then(resp => talksUI(resp)) //The data is sent to the function to be displayed in the UI
    .catch(error => console.log('ERROR!'));
}

//Tabs control: yesterday, today, tomorrow
function onTabClick(e){
    e.preventDefault();
    let activeTabs = document.querySelectorAll('.active');

    activeTabs.forEach(tab => {
        tab.className = tab.className.replace('active', '');
    });

    event.target.className += ' active';
    document.getElementById(e.target.href.split('#')[1]).className += ' active';
}
